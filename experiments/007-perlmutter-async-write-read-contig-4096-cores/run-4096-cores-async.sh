#!/bin/bash
#SBATCH -C gpu
#SBATCH -A m2621_g
#SBATCH -q regular
#SBATCH -t 00:30:00
#SBATCH --ntasks-per-node=64
#SBATCH -N 64
#SBATCH -n 4096

module swap PrgEnv-nvidia PrgEnv-gnu
module load python

./h5bench --debug configuration-4096-async.json
