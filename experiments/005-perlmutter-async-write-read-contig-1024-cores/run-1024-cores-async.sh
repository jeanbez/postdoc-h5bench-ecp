#!/bin/bash
#SBATCH -C gpu
#SBATCH -A m2621_g
#SBATCH -q regular
#SBATCH -t 00:30:00
#SBATCH --ntasks-per-node=64
#SBATCH -N 16
#SBATCH -n 1024

module swap PrgEnv-nvidia PrgEnv-gnu
module load python

./h5bench --debug configuration-1024-async.json
