#!/bin/bash
#SBATCH -C gpu
#SBATCH -A m2621_g
#SBATCH -q regular
#SBATCH -t 00:30:00
#SBATCH --ntasks-per-node=64
#SBATCH -N 32
#SBATCH -n 2048

module swap PrgEnv-nvidia PrgEnv-gnu
module load python

./h5bench --debug configuration-2048.json
